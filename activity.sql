--1.

SELECT * FROM customers WHERE country Like "Philippines";

--2.

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

--3.

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

--4.

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

--5.

SELECT customerName FROM customers WHERE state is NULL;

--6.

SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" && lastName = "Patterson";

--7.

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" && creditLimit > 3000;

--8.

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

--9.

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

--10.

SELECT DISTINCT country FROM customers;

--11.

SELECT DISTINCT status FROM orders;

--12.

SELECT customerName, country FROM customers WHERE country IN ("USA", "FRANCE", "Canada");

--13.

SELECT employees.firstName, employees.lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";

--14.

SELECT customers.customerName, employees.firstName, employees.lastName FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

--15.

SELECT products.productName, customers.customerName FROM customers 
    JOIN orders ON customers.customerNumber = orders.customerNumber 
    JOIN orderdetails ON orders.orderNumber =  orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode
    WHERE customers.customerName = "Baane Mini Imports";

--16.

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
    JOIN offices ON customers.country = offices.country
    JOIN employees on offices.officeCode = employees.officeCode
    WHERE offices.country = customers.country;

--17.

SELECT productName, quantityInStock FROM products WHERE productLine = 'planes' AND quantityInStock < 1000;

--18.

SELECT customerName FROM customers WHERE phone LIKE "%+81%";